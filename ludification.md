# Ludification

## Intro

La ludification, plus couramment utilisé sous l'anglicanisme **Gamification** est l'utilisation des mécanisme du jeu dans d'autres domaines.
Ce principe doit être compris et utilisé au sein de la distribution **Primtux** afin d'acroire au mieux l'implication des élèves, des professeurs et des parents.

Elle est bien décrite sur [wikipédia](https://fr.wikipedia.org/wiki/Ludification).

Attention, c'est un moyen comme un autre pour favoriser la réussite scolaire et n'est en aucun cas une fin en soit.
Tout ne peux pas être ludifié pour diverses raisons.
