# Intro

Une objection régulière quant à l'adoption de Primtux est de privilégier les tablettes aux desktops/laptops (ordis fixes/ordis portables).
L'objectif de cette section est de dénombrer les forces et les contre-arguments en faveur de l'adoption à Primtux.
Pour beaucoup de points, c'est une redite de [l'adn de Primtux](../adn_primtux.md).
Les arguments n'ayant pas le même poids selon l'interlocuteur, nous pouvons identifier un certains nombres de cibles caractérisé par des personnas :

- **P1** : Les élus locaux, maires etc.
- **P2** : Les ERUN (Enseignants pour les Ressources et les Usages Numériques), installateurs, mainteneurs.
- **P3** : Les professeurs des écoles, ASP (Assistant Scolaire Personnalisé) et directeur.
- **P4** : Les élèves.
- **P5** : Les parents et représentant des parents d'élèves. (aussi bien en usage dommestique que dans leur faculté à faire confiance aux choix pédagogiques pour leurs enfants)

Le ou les personas concernés seront annotés dans les différents arguments suivants.

# Les attentes des différents protagonistes :

**P1** :

- Les coûts.
- Faire travailler des entreprises locales pour l'installation/maintenance/formation.
- Utiliser des produits made in France.
- Que ça soit beau et novateur.
- Impacte écologique grâce au recyclage de matériel.
- Adapté aux Dys et aux enfants en situation de handicap.

**P2** :

- Simple à récupérer, installer, configurer, automatiser, personnaliser et dépanner.
- Peu de bugs.
- Documentation soignée et à jour.
- Communauté ou service client réactif et compétent pour de l'entraide.
- Personnalisation adaptée pour conserver ou revaloriser du matériel viellissant.

**P3** :

- Simple d'utilisation et autonomie des élèves.
- Graphiquement abouti : l'estétique est apaisant et rassurant.
- Formation pour une prise en main optimal.
- Peu de bugs, de latence et de paramètrage côté élève.
- Large choix d'applications éducatives choisis pour correspondre au programme scolaire par cycle d'apprentissage.
- Contenu ludique et stimulant.
- Pas de possibilité d'accès à du contenu inaproprié.

**P4** :

- Contenu ludique et stimulant.
- Permet une autonomie rapide.
- Adapté à son niveau.
- Adapté à mon handicap.

**P5** :

- Pas de possibilité d'accès à du contenu inaproprié.

# Arguments en faveur des tablettes :

## Simple à utiliser :

- Correcteur et auto-complétion intégrés.
- Interface tactile facile à prendre en main.
- Applications nombreuses et faciles à installer/supprimer disponibles via des stores.

## Proche des usages du quotidien :

- la plupart des enfants et des professeurs ont une tablette à la maison et sont habilités pour l'utiliser.

## Interfaces léchées

- design soigné ainsi que l'UI/UX.

# Arguments en faveur de Primtux :

## Les objectifs pédagogiques (**P3, P4, P5**) :

- Développé et/ou conseillé par des **professeurs des écoles**, **ERUN**.
- Conforme aux programmes scolaires dans la francophonie.
- Favoriser le droit à l'éducation : (https://fr.wikipedia.org/wiki/Droit_%C3%A0_l%27%C3%A9ducation)[https://fr.wikipedia.org/wiki/Droit_%C3%A0_l%27%C3%A9ducation].
- Soutenir l'éducation pour tous (https://fr.unesco.org/themes/education-pour-tous)[https://fr.unesco.org/themes/education-pour-tous].
- Lutter contre la fracture numérique.

## Simple à utiliser :

- Applications nombreuses, faciles à installer/supprimer disponibles.

## Verrouillages des paramètrages avancés (**P3**) :

Dans la plupart des tablettes, il est assez simple pour un élève suffisament maladroit ou mal attentionné de détraquer des paramètres de la tablette, d'installer ou désinstaller des applications.
La remise en état peut être plus ou moins complexe et coûteuse en temps pour le personnel encadrant.

**Primtux** vérouille se genre de manipulation en amont pour les élèves.

## La cohérence globale (**P1, P3**) :

**Primtux** est une distribution dédié à l'école dans le cadre des 3 premiers cycles du primaire.
Il est donc étudié pour être au plus proche des besoins dans ce cadre précis.

Une tablette de type Ipad, Android etc. n'a pas cette vocation : elle couvre un éventaille beaucoup plus large et par conséquent est bien moins adapté à la cible :

- Fonctionnalités et logiciels inutiles ou innapropriés.
- Trouver le bon exerciseur à la charge de l'enseignant.

## Maintenance (**P2, P5**) :

- Facilité et efficacité de l’entretien du parc de machines et du système installé.

## Nébuleuse autours des droits d'auteurs (**P1, P2**) :

Sur les tablettes, il est souvent très délicat de savoir si un contenu ou une application est autorisé dans le cadre scolaire.
L'équipe de Primtux veille avec soin à ne rendre disponible que des outils qui rentre dans des clauses légales d'utilisation.

## Vie privé (**P1**) :

**Primtux** ne collecte aucune information et veille à protéger au maximum les utilisateurs par l'intermédiaire d'un contrôle parental paramètrable.
Il évite les pubs, tous contenus non adaptés aux mineurs et toute source de distraction.

Même derrière des proxys filtrants, des pare-feux, DNS menteurs etc, certaines tablettes contournent volontairement pour des objectifs de traçabilité.

## Indépendance et Souveraineté numérique (**P1**) :

Il est important de comprendre que les sociétés derrières les systèmes d'exploitations de tablette (Google, Apple etc.) :

- Ont leur siège aux Etats-Unis ou en Chine et sont muts aux enjeux politique de ses super-puissances.
- Font de l'évasion fiscale et par conséquent appauvrisse les économies locales.
- Réfléchissent au bénéfice avant tout : donc si le contenu n'est pas adapté à l'éducation mais qu'il est lucratif, ils ne le changeront pas.
Si, au contraire, un outil n'est plus lucratif, ils peuvent décider de le supprimer.
- Aucun moyen de réguler et dicter leurs choix.

- **Primtux** est développé et maintenu en France par les membres d'une association loi 1901 ainsi que par des sympatisants.

## La durée de vie (**P1, P2**) :

- Primtux veille à rester compatible sur des périphériques très anciens afin d'augmenter leur durée de vie.
- En règle général, un ordinateur (desktop d'avantage que laptop) est plus modulaire qu'une tablette.
 Ce qui veut dire que si un élément venait à être défectueux (ram, disque dur, écran, batterie etc.), il est bien plus aisé de ne changer que cette pièce plutôt qu'un remplacement complet.
- Les tablettes ont tendances à être plus fragiles (ex : chute et écran cassé).

## Environnement

**Primtux** est réfléchi avant tout pour du reconditionnement, pour prolonger la vie du matériel et pour limiter le gaspillage entraîné par la surconsommation de matières premières.

Le but est donc d'éviter le gaspillage :

- de RAM
- de consommation de CPU/GPU
- de place sur le disque
- d'écritures et lectures sur des périphériques
- de consommation de bande passante (en particulier internet)
- de consommation électrique, en particulier des batteries dans le cadre des ordinateurs portables
- de matériel considéré comme obsolètes
- en évitant si possible les services "Cloud" (souvent des coûts énergétiques cachés)

## Preparation à des usages professionnels(**P3, P4**) :

Une tablette est avant tout un outil dédié d'avantage à de la consommmation plutôt qu'à de la création. (Dépourvu de clavier physique et de souris). 

## Le coût (**P1, P2**) :

- **Primtux** est entièrement développé dans un cadre associatif et bénévole (pas d'entité économique derrière).
- Il n'y a donc pas de frais d'installation, de licences etc.
- Lutte au maximum contre la fracture du numérique.

## Ethique (**P1, P5**) :

- Favoriser le droit à l'éducation : (https://fr.wikipedia.org/wiki/Droit_%C3%A0_l%27%C3%A9ducation)[https://fr.wikipedia.org/wiki/Droit_%C3%A0_l%27%C3%A9ducation].
- Soutenir l'éducation pour tous (https://fr.unesco.org/themes/education-pour-tous)[https://fr.unesco.org/themes/education-pour-tous].

## fonctionnement en mode offline (**P3**) :

- Plusieurs écoles ont encore des soucis permanents ou temporaires d'accès à Internet (hors ligne, coupures, débits insuffisents).
La plupart des applications sur tablettes nécessite des connexions permanentes et il est délicat de savoir leur réaction en cas de coupure, de lenteurs etc.

**Primtux** a pris le parti pris de proposer une large gamme d'applicatifs tout simplement disponible sans accès Internet.

## Convient à des usages hybrides (**P2, P3**) :

Rien n'interdit d'avoir des tablettes et des ordinateurs fixes.

**Primtux** fourni des applications web qui peuvent donc être également utilisé sur tablette (1 poste sous Primtux faisant office de serveur web).

# Synthèse :

La solution **Primtux**, bien qu'imparfaite a de nombreux atouts par rapport à des tablettes pré-conditionnés.
Elle est disponible à un prix imbatable tout en remplissant quasiment toutes les cases en faveur des besoins de l'enseignement.

Il est évident que les interfaces tactiles soient ce qui se fait de plus naturel pour une prise en main (en particulier le 1er cycle).
**Primtux** n'est pas ce qui est de plus adapté pour ces interfaces mais l'écart se réduit : il existe de plus en plus de matériel tactile pour desktop/laptop, les coûts diminues et le support Linux se perfectionne.

Il est adapté aux problématiques du terrain (faible budget, enfants en situation de handicap, soucis internet etc.) et non à une éducation idéalisé.

Bien évidement, le but des auteurs de **Primtux** n'est pas de révolutionner les usages.
Si des tablettes sont déjà en place et utilisés intelligement, il est tout à fait contre productif de tout remplacer.
Néanmoins, des usages hétérogènes sont tout à fait envisageables.

La distribution **Primtux** (et l'association derrière) a la volonté d'enrichir et perfectionner l'apprentissage au travers de moyens numériques les plus adaptés.
Ceci au détriment de toute volonté mercantile et politique.

Elle est pour l'instant peu connu mais ces utilisateurs en sont grandement satisfait.
