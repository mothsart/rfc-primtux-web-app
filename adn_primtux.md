# Rappel de l'ADN de Primtux

1. **Orienté Pédagogie**.
Ce qui sous-entend :
    - Pas de pubs ni de contenus inapropriés.
    - Pas de distraction.
    - Développé et/ou conseillé par des **professeurs des écoles**, **ERUN**.
    - Conforme aux programmes scolaires dans la francophonie.
    - Favoriser le droit à l'éducation : (https://fr.wikipedia.org/wiki/Droit_%C3%A0_l%27%C3%A9ducation)[https://fr.wikipedia.org/wiki/Droit_%C3%A0_l%27%C3%A9ducation].
    - Soutenir l'éducation pour tous (https://fr.unesco.org/themes/education-pour-tous)[https://fr.unesco.org/themes/education-pour-tous].
    - Lutter contre la fracture numérique.
    - Qu'il est avant tout pertinent d'un point de vue éducatif.
    - Est construite autour des programmes éducatifs de l'éducation nationale.

2. **Indépendant et associatif** : pas d'entité économique derrière.
Ce qui sous-entend que Primtux ne fera jamais le jeu de s'accoquiner avec des entreprises aux valeurs mercantiles.

Il peut privilégié certains outils créé par des entreprises mais seulement car ces derniers correspondent au mieux à son objectif n°1 (la pédagogie).

3. **Simple et pratique** :
    - Réflexion autours de l'UI et l'UX (expérience et ressenti utilisateur) : **intuitif**, **attractif**, **crédible** et **efficace**.
    - Contenu adapté à chacun des cycles.
    - Pas de mot de passe pour les enfants.

4. **Légereté et réactivité** : la distribution et ses logiciels sont adaptés à du matériel de récupération ainsi qu'à des nano-ordinateur. (raspberry pi)

5. **La Sécurité** : basé sur Linux donc moins vulnérables aux virus, malwares, spywares, ransomwares etc.
Le développement de nos outils rentre également dans des contraintes inhérente à ses enjeux.

6. **La Gratuité** : pas de frais d'installation ni de mise à jour.

7. **La Personnalisation** : Car chaque besoin est différent.
Utilisation possible dans le cadre scolaire, familiale, d'une ludothèque, pour des enfants DYS etc.

De même, Primtux est installable sur des ordinateurs traditionnels mais également sur les nanos ordinateurs "Raspberry PI"

8. **Documenté** : wiki à jour, documents offlines, aide interactive et marques-pages scrupuleusement sélectionnés pour étendre les besoins pédagogiques.

9. **Adapté au support** : web app "responsives", logiciels dédié au TBI (Tableaux interactifs).

10. **Filtrage** : possibilité de mettre en place un contrôle parental (CTParental) et moteur de recherche Qwant Junior.

11. **Durable** : Durée de vie prolongée avec un support sur le long terme.

12. **Cohérence** : une vrai réflexion sur les outils choisis mis en place, les interactions entre eux et leur organisation.

13. **Adapté** à différentes formes de handicap : Troubles DYS, trouble de la vision, de l'audition etc.

14. **Stabilité** : basé sur Debian et Ubuntu, le système est robuste.

15. **Interactivité** : Forum actif.

16. **La Liberté** : le code, sous **licences libres** peut être étendu, modifié ainsi que la plupart des contenus textuels, graphiques etc.
Celà sous-entend aussi que Primtux mettra toujours en avant des logiciels (et contenus) ouverts.

17. **La transparence** : **Primtux** s'engage à ne pas traiter les données personnelles générées dans le cadre de l'utilisation de ses services.

18. **Communication** : info sur Twitter, régulièrement sur linuxfr.org, developpez.com, dans des magazines de référence.

19. **Ludique** : rien de mieux que d'apprendre en s'amusant !
Néanmoins cette règle doit toujours s'inscrire dans son objectif n°1 : ludique dans la mesure du possible mais pédagogique avant tout.
Primtux n'est pas là pour divertir mais pour éveiller la curiosité, apprendre et consolider ses acquis.

20. **Eco responsable** : **Primtux** est réfléchi avant tout pour du reconditionnement, pour prolonger la vie du matériel et pour limiter le gaspillage entraîné par la surconsommation de matières premières.
Le but est donc d'éviter le gaspillage (et rentre pleinement dans une démarche de **responsabilité écologique**) :
    - De la RAM.
    - De consommation de CPU/GPU.
    - De place sur le disque dur.
    - Du nombre d'écritures et lectures sur des périphériques.
    - De la consommation de bande passante (en particulier internet).
    - De la consommation électrique, en particulier des batteries dans le cadre des ordinateurs portables.
    - En priviliégiant du matériel considéré comme obsolètes.
    - En évitant si possible les services "Cloud" (souvent des coûts énergétiques cachés).

Loin du green washing, Primtux veille a maintenir cette règle de manière pragmatique et désintéressé.

21. **Pragmatisme et souplesse** : Même si l'objectif 16 (de liberté) est primordiale, les mainteneurs de Primtux sont conscient que les besoins et les habitudes pédagogiques nécessitent d'utiliser des outils propriétaires.

**Primtux** rend possible d'utiliser un certain nombre de logiciels non libres pour garder de la souplesse dans les usages.

Dans le même état d'esprit, l'équipe a constaté que les accès Internet sont souvent disparatre sur le territoire et que ça peut être pénalisant pour répondre à l'objectif n°1 (la pédagogie).
Pour réduire cet écart, Primtux privilégie l'utilisation **off-line** (hors-ligne donc sans accès ou accès dégradé à Internet).
