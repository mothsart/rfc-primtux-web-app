# Lexique

## RFC

https://fr.wikipedia.org/wiki/Request_for_comments

Un cahier des charges 

## NSFW

https://fr.wiktionary.org/wiki/NSFW

Dans le cadre de Primtux, ces intiales représentent tous les contenus non appropriés à des enfants.

## BEM

Règles de nommage en CSS permettant une orientation composant ainsi que des performances prodigieuses.
(Le DOMCSS est très peu parcouru)

## SEO

https://fr.wikipedia.org/wiki/Optimisation_pour_les_moteurs_de_recherche#:~:targetText=L'optimisation%20pour%20les%20moteurs,pour%20search%20engine%20result%20pages).

Ensemble de techniques mise en place pour que le contenu d'un site web soit le mieux référencé par un moteur de recherche.

## UI

https://fr.wikipedia.org/wiki/Interface_utilisateur

## UX

https://fr.wikipedia.org/wiki/Exp%C3%A9rience_utilisateur

## UNIX

La philosophie d'UNIX :

- Écrivez des programmes qui effectuent une seule chose et qui le font bien.
- Écrivez des programmes qui collaborent.
- Écrivez des programmes pour gérer des flux de texte [en pratique des flux d'octets], car c'est une interface universelle.
(Par exemple HTML)

Plus de détail : https://fr.wikipedia.org/wiki/Philosophie_d%27Unix#:~:targetText=La%20philosophie%20d'Unix%20est,du%20syst%C3%A8me%20d'exploitation%20Unix.

## KISS

https://fr.wikipedia.org/wiki/Principe_KISS#:~:targetText=Keep%20it%20simple%2C%20stupid%20(en,toute%20la%20mesure%20du%20possible.

On préconise la simplicité.

## i18n

Abréviation pour "internationalisation".
S'utilise qu'en un programme est dans la mesure de passer d'une langue à une autre et qu'elle peut être traduite sans intervention d'un technicien.
(uniquement des outils de traduction tel que poedit)

## a11y

Abréviation pour "accessibilité".
Rassemble toutes les techniques mis en place pour q'un applicatif soit adapté à des personnes souffrant d'un handicap. (aussi bien physique que intellectuel)

## DYS

Troubles d'apprentissage désignant 6 pathologies : https://www.ffdys.com/troubles-dys

## cypress

Outil permettant de tester le rendu et les actions utilisateurs à l'aide scénarios.

## CI

Intégration Continue

