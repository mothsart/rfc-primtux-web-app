# README

# Intro

Ce cahier des charges ou plus précisément [RFC](./lexique.md#rfc) permet de définir les objectifs et les exigences de nos applis créés en interne (au sein de l'association Primtux).

Il est segmenté en **3 points** :
- Ce qui est valables dans nos applis actuellement (**1.0 donc standard**).
- Ce qui est en cours d'étude (**1.x : Brouillon**).
- Ce qu'il faudrait prévoir (**2.0 : propositions**).

Il utilise les termes en vigueur dans une [RFC](./lexique.md#rfc) soit :

- **DOIT** pour les obligations (**MUST**).
- **NE DOIT PAS** pour les interdictions (**MUST NOT**).
- **DEVRAIT** pour les recommendations (**SHOULD**).
- **PEUT** pour les compromis (**MAY**).

# **1.0** : Ce qui est valable dans nos applications :

- une application **DOIT** servir un besoin pédagogique précis et un seul :
    - On limite les efforts d'un produit trop imposant donc long à produire et à maintenir.
    - On respecte des principe [KISS](./lexique.md#kiss) et [UNIX](./lexique.md#unix).

- une application **NE DOIT PAS** contenir :
    - De contenu publicitaire.
    - De contenu [NSFW](./lexique.md#nsfw).

Néanmoins, certaines choses **PEUVENT** arriver à notre insus. (exemple : une phrase qui peut être avoir plusieurs interprétations)
Si le besoin s'en fait sentir, nous changerons.

L'application **DOIT** vivre le plus longtemps possible sans interventions humaines une fois livré.

- Prérequis technique :
    - Une application web car les technos webs sont universelles (non propriétaire à une entreprise), existent depuis longtemps et changent peu.
    Il n'y a que très peu de fonctionnalités qui sont dépréciés.

- L'application **DOIT** pouvoir être écrite et corrigé dans des temps convenables.

- L'application **DOIT**, dans la mesure du possible être utilisable en mode "offline".

# **1.x** : ce qui pourrait être valable dans nos applications :

- Une application dédié à des élèves **DEVRAIT** être [ludique](./ludification.md).

# 2.0 : propositions de ce qui pourrait être valable dans nos applications :

- On **DEVRAIT** cloisonner :
    - Le contenu.
    - La mise en forme.
    - La structure.
    - Les algorithmes.

- [I18n](./lexique.md#i18n) : Même si nos applis sont principalement dédiés au français et par extension au milieu francophone, nous **DEVRIONS** réfléchir à la possibilité de les traduire.
Plus cette question est soulevé tôt et plus l'investissement est faible.
Rien n'interdit que les efforts de Primtux soient visibles un jour ou l'autre en dehors de nos frontières, que quelqu'un souhaite la traduire.

- Afin de maitriser les applications réalisées, leur évolution et leur maintenance, il **DEVRAIT** y avoir des tests automatisés :
   - Unitaires
   - Fonctionnels : Scénarios les plus courants avec [cypress](./lexique.md#cypress).

- Design :
    - Choix des couleurs : https://whocanuse.com/tter.

- On devrait privilégié une orientation **Composants** (morceaux d'interface réutilisables) afin de garantir :
    - Une maintenance plus aisé.
    - Une uniformisation des comportements (Ux).
    - Une optimisation des ressources et des performances.

- On **DEVRAIT** utiliser des principes de [DRY](./lexique.md#dry).

- Optimisation :
    - Du DOM.
    - Des assets : Images, fichiers SVG etc.
    - Des requètes HTTP.
    - Des scripts dans la norme Ecmascript.
    - Du CSS : utilisation de [BEM](./lexique.md#bem).

- L'application **DOIT** idéalement avoir une cible la plus grande possible :

    - Une application **web** semble être le choix à privilégié car couvront au mieux ce besoin.

- On **DEVRAIT** proposer des applications **responsives**.

- On **DEVRAIT** soigner la [SEO](./lexique.md#seo).

- [A11y](./lexique.md#a11y) et [Dys](./lexique.md#dys) : Les applications **DOIVENT** être adaptées pour un maximum de handicaps.

- Obtenir un badge [CII](https://bestpractices.coreinfrastructure.org/fr).
