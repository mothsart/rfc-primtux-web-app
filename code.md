# Code

## Intro

Afin de simplifer la relecture et le partage du code, il est impératif de déterminer des règles communes.

1. Indentation de **4 espaces**. (pas de tabulation)

2. **UTF-8** dans la mesure du possible.

3. code et URL en **ascii** : pas de variables avec des caractères accentués ou autre excentricité.
Il a des vrais raisons derrières : on évite des soucis d'encodage et de sécurité.

4. pas d'obligation d'écrire le code en anglais mais si possible les éléments redondants : hidden, disable etc.

5. constantes en **MAJUSCULE**.

6. nom des variables en **snake_case**.

7. Nom des structures, classes et fonctions en **camelCase**.

8. Nom des id HTML en **kebab-case**. (on évite les id non valides)

9. Nom des classes CSS respectant **BEM**.

10. Règle de sécurités : éviter les injections SQL, XSS etc.
